import React from "react";
import { Layout, Menu, Typography } from "antd";

const { Header } = Layout;
export default function Head() {
  return (
    <Header className="header">
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={"1"}
        onClick={(e) => console.log(e.key)}
      >
        <Menu.Item key="1">Home</Menu.Item>
        <Menu.Item key="2">Contact Us</Menu.Item>
      </Menu>

      <Typography.Title style={{ color: "white" }}>Logo</Typography.Title>
    </Header>
  );
}
