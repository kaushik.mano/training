import React from "react";
import { Layout, Menu, Button } from "antd";
import Intro from "./intro";
import Teams from "./team";
import Portfolio from "./portfolio";
import Location from "./location";
const { Sider, Content, Footer, Header } = Layout;
const { SubMenu } = Menu;

export default function Home() {
  return (
    <Content style={{ padding: "0 50px" }}>
      <Layout className="site-layout-background" style={{ padding: "24px 0" }}>
        <Sider className="site-layout-background" width={200}>
          <Menu
            mode="inline"
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            style={{ height: "100%" }}
            onClick={(e) => {
              document.getElementById(e.key).scrollIntoView({
                behavior: "smooth",
                block: "nearest",
              });
              console.log(e.key);
            }}
          >
            <Menu.Item key="intro">Introduction</Menu.Item>
            <Menu.Item key="portfolio">Portfolio</Menu.Item>
            <Menu.Item key="team">The Team</Menu.Item>
            <Menu.Item key="location">Location</Menu.Item>
          </Menu>
        </Sider>
        <Content style={{ padding: "0 24px", minHeight: 280 }}>
          <Intro />
          <Teams />
          <Portfolio />
          <Location />
          <Button type="default">Hello</Button>
        </Content>
      </Layout>
    </Content>
  );
}
