import React from "react";
import { Card, Row, Col, Typography } from "antd";
export default function Teams() {
  return (
    <div className="site-card-wrapper" id="team">
      <Typography.Title level={3}>A sample Teams cards</Typography.Title>
      <Row gutter={16}>
        <Col span={8}>
          <Card title="Member One" hoverable>
            First memeber
          </Card>
        </Col>
        <Col span={8}>
          <Card title="Member One" hoverable>
            First memeber
          </Card>
        </Col>

        <Col span={8}>
          <Card title="Member One" hoverable>
            First memeber
          </Card>
        </Col>
      </Row>
    </div>
  );
}
